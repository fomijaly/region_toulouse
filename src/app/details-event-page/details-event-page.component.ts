import { Component, Input } from '@angular/core';
import { ButtonComponent } from '../shared/buttons/button/button.component';
import { Event } from '../shared/list-activities/list-activities.component';
import { EventService } from '../event.service';
import { ActivatedRoute, ParamMap, RouterLink } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-details-event-page',
  standalone: true,
  imports: [ButtonComponent, RouterLink],
  templateUrl: './details-event-page.component.html',
  styleUrl: './details-event-page.component.css'
})
export class DetailsEventPageComponent {
  eventId?: string;
  event?: Event;
  formattedSentence?: string;

  constructor(
    public eventService: EventService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    const paramMap: ParamMap | null = this.route.snapshot.paramMap;
    if (paramMap !== null) {
      const eventId = paramMap.get('identifiant');
      if (eventId !== null) {
        this.eventId = eventId;
      }

      this.eventService.getDetailEvent(this.eventId).then((data) => {
        this.event = data[0];
        if (this.event !== undefined) {
          this.formattedSentence = this.formatDescriptif(
            this.event.descriptif_long
          );
        }
      });
    }
  }
  formatDescriptif(text: string): string {
    return text ? text.replace(/\n/g, '<br>') : '';
  }

  back(): void {
    this.location.back();
  }
}
