import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { Event } from './event';


@Pipe({
  name: 'searchBar',
  standalone: true
})
@Injectable({ providedIn: 'root' })
export class SearchBarPipe implements PipeTransform {

  transform(eventList: Event[], searchTerms: string): Event[] {

      // on convertit la string en input pour en faire un tableau d estring, que l'on va parcourir pour trouver une correspondance
      const searchTermsArray = searchTerms.toLowerCase().split(" ");

      const filteredEventsByTerms = eventList.filter(event => {

        // on convertit l'objet event en tableau de string, plus facile à parcourir
        // Object.values() permet d'obtenir les valeurs des propriétés de chaque objet sous forme de tableau

        const eventPropertyValueArray = Object.values(event).map(value => {

          // si la propriété est de type string, on lowerCase, sinon on convertit en string
          return typeof value === 'string' ? value.toLowerCase() : '';
        
        })

       // Utilisation de .every() pour vérifier que chaque terme de recherche est présent dans les propriétés de l'événement.
      // Pour chaque terme de recherche dans searchTermsArray, .some() vérifie si ce terme est inclus dans au moins
      // une des propriétés de l'événement. Si .some() retourne true, ça signifie que ce terme est trouvé dans au moins une
      // des propriétés de l'événement. Pour que l'événement soit inclus dans filteredEventsByTerm, .every() doit retourner true
      // pour tous les termes de recherche

        return searchTermsArray.every(searchTerm =>
          eventPropertyValueArray.some(eventPropertyValue => 
            eventPropertyValue.includes(searchTerm)
          )
        );
      })
    console.log("PIPE ", filteredEventsByTerms);
    
    return filteredEventsByTerms;
  }
}
