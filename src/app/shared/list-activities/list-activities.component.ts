import { Component, Input, SimpleChanges } from '@angular/core';
import { EventCardComponent } from '../event-card/event-card.component';
import { EventService } from '../../event.service';
import { Event } from '../../event';
import { CommonModule } from '@angular/common';
import { SearchBarPipe } from '../../search-bar.pipe';
import { Month } from '../../month';


@Component({
  selector: 'app-list-activities',
  standalone: true,
  imports: [
    EventCardComponent,
    CommonModule,
    SearchBarPipe
  ],
  templateUrl: './list-activities.component.html',
  styleUrl: './list-activities.component.css'
})
export class ListActivitiesComponent {

  eventList : Event[] = [];
  filteredEventList : Event[] = [];
  keyOfDate? : string;
  currentMonth?: string;

  @Input() selectedCategoryFilter?: string;
  @Input() selectedCityFilter?: string;
  @Input() selectedDateFilter?: Date;
  @Input() searchTerms: string = '';


  constructor(private eventService: EventService) {}

  ngOnInit(): void {
    this.eventService.getEvents().then(events => {
      // console.log(events)
      this.eventList = events.results;
      this.filterEvents()

      const currentdateEvent = this.eventList[0].date_debut.toString().split('-');
      const currentMonth = currentdateEvent[1]

      for(const key in Month){
        if(key == currentMonth){
          this.keyOfDate = Month[key];
        }
      }

    })
  }

  filterEvents(){

    let filteredEvents = this.eventList;
    if(this.selectedCategoryFilter){
      filteredEvents = filteredEvents.filter(event => event.categorie_de_la_manifestation === this.selectedCategoryFilter)
    } 
    if(this.selectedCityFilter){
      filteredEvents = filteredEvents.filter(event => event.commune === this.selectedCityFilter)
    }
    if(this.selectedDateFilter){
      filteredEvents =  filteredEvents.filter(event => event.date_debut === this.selectedDateFilter)
    }
    this.filteredEventList = filteredEvents
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['selectedCityFilter'] || changes['selectedDateFilter'] || changes['selectedCategoryFilter']) {
      this.filterEvents();
    }
  }
}




export { Event };

