import { Component, Input } from '@angular/core';
import { Event } from '../list-activities/list-activities.component';
import { ButtonComponent } from '../buttons/button/button.component';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-event-card',
  standalone: true,
  imports: [
    RouterLink,
    ButtonComponent
  ],
  templateUrl: './event-card.component.html',
  styleUrl: './event-card.component.css'
})
export class EventCardComponent {
  @Input()
  event?: Event;
}
