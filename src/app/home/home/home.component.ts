import { Component } from '@angular/core';
import { EventCardComponent } from '../../shared/event-card/event-card.component';
import { ButtonComponent } from '../../shared/buttons/button/button.component';
import { BannerComponent } from '../../shared/banner/banner.component';
import { ListActivitiesComponent } from '../../shared/list-activities/list-activities.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    EventCardComponent,
    ButtonComponent,
    BannerComponent,
    ListActivitiesComponent
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {}
