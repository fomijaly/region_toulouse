import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ListActivitiesComponent } from '../shared/list-activities/list-activities.component';
import { EventService } from '../event.service';
import { Event } from '../event';
import { ButtonComponent } from '../shared/buttons/button/button.component';
import { FormsModule } from '@angular/forms';
import { MapComponent } from '../map/map.component';
import { SearchBarComponent } from '../shared/search-bar/search-bar.component';


@Component({
  selector: 'app-event-list',
  standalone: true,
  imports: [
    CommonModule,
    ListActivitiesComponent,
    ButtonComponent,
    FormsModule,
    MapComponent,
    SearchBarComponent
  ],
  templateUrl: './all-events.component.html',
  styleUrl: './all-events.component.css'
})

export class AllEventsComponent {

  constructor(private eventService: EventService) {}

  villes: string[] = [];
  categories: string[] = [];
  selectedCity: string = '';
  selectedDate?: Date;
  selectedCategory: string = '';
  showAllCategories: Boolean = false;
  searchTerms: string = '';


  ngOnInit(): void {
    this.eventService.getEvents().then(events => {
      this.categories = this.getCategories(events.results);
      this.villes = this.getCities(events.results);
    });
  }

  getCities(events: Event[]){
    let allCities: string[] = [];
    events.forEach(event => {
      const city = event.commune;
      if (city && !allCities.includes(city)) { // Vérifie si la ville n'est pas déjà dans le tableau
        allCities.push(city); // Ajoute la ville si pas déjà présente
      }
    });
    console.log(allCities)
    return allCities
  }

  getCategories(events: Event[]){
    let allCategories: string[] = [];
    events.forEach(event => {
      const category = event.categorie_de_la_manifestation;
      if (category && !allCategories.includes(category)) {
        allCategories.push(category);
      }
    });
    console.log(allCategories)
    return allCategories
  }


  applyCityFilter(city: string): void {
    console.log(this.selectedCity)
    this.selectedCity = city;
  }
 

  applyDateFilter(date: Date | undefined): void {
    this.selectedDate = date;
  }

  applyCategoryFilter(category: string): void {
    if (this.selectedCategory === category) {
      this.selectedCategory = '';
    } else {
      this.selectedCategory = category;
    }
    console.log(this.selectedCategory)
  }

  toggleShowAllCategories() {
    this.showAllCategories = !this.showAllCategories;
  }

  onSearchChange(newSearchTerms: string) {
    this.searchTerms = newSearchTerms;
  }
}
